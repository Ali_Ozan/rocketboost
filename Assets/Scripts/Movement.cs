using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] float thrustPower = 500f;
    [SerializeField] float rotationPower = 100f;
    [SerializeField] float correctorIntensity = 2f;
    [SerializeField] AudioClip mainEngine;
    [SerializeField] AudioSource leftBoosterAudio;
    [SerializeField] AudioSource rightBoosterAudio;
    [SerializeField] ParticleSystem mainThrusterParticles;
    [SerializeField] ParticleSystem leftThrusterParticles;
    [SerializeField] ParticleSystem rightThrusterParticles;

    private Rigidbody rigidBody;
    private AudioSource thrustAudio;

    private float horizontal;
    private float vertical;


    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        thrustAudio = GetComponent<AudioSource>();
    }

    void Update()
    {
        ProcessThrust();
        ProcessRotation();
    }

    void ProcessRotation()
    {
        //user control
        horizontal = Input.GetAxis("Horizontal");
        rigidBody.AddTorque(new Vector3(0f, 0f, -1f) * horizontal * rotationPower * Time.deltaTime);
        AdjustRotatorEffects();
        ApplyCorrectionForce();
    }
    void ProcessThrust()
    {
        vertical = Input.GetAxis("Vertical") > 0 ? Input.GetAxis("Vertical") : 0f;
        if (vertical > 0.0001f && !mainThrusterParticles.isPlaying)
            mainThrusterParticles.Play();
        if (vertical < 0.0001f && mainThrusterParticles.isPlaying)
            mainThrusterParticles.Stop();

        rigidBody.AddRelativeForce(new Vector3(0f, 1f, 0f) * vertical * thrustPower * Time.deltaTime);
        PlayAudio();
    }

    private void ApplyCorrectionForce()
    {
        //correction force for smoother/better gameplay
        float rotationOffset = -rigidBody.transform.rotation.eulerAngles.z;
        rotationOffset = Mathf.Abs(rotationOffset) > 180f ? 360f - Mathf.Abs(rotationOffset) : rotationOffset;
        Vector3 correctorVector = new Vector3(0f, 0f, rotationOffset);
        rigidBody.AddTorque(correctorVector * correctorIntensity * Time.deltaTime);
    }

    private void AdjustRotatorEffects()
    {
        if (horizontal > 0.0001f && !leftThrusterParticles.isPlaying)
        {
            leftThrusterParticles.Play();
            leftBoosterAudio.Play();
        }
        if (horizontal < 0.0001f && leftThrusterParticles.isPlaying)
        {
            leftThrusterParticles.Stop();
            leftBoosterAudio.Stop();
        }

        if (horizontal < -0.0001f && !rightThrusterParticles.isPlaying)
        {
            rightThrusterParticles.Play();
            rightBoosterAudio.Play();
        }
        if (horizontal > -0.0001f && rightThrusterParticles.isPlaying)
        {
            rightThrusterParticles.Stop();
            rightBoosterAudio.Stop();
        }
    }


        private void PlayAudio()
    {
        if (vertical > 0.00001f && !thrustAudio.isPlaying)
            thrustAudio.PlayOneShot(mainEngine);
        else if (vertical < 0.00001f && thrustAudio.isPlaying)
            thrustAudio.Stop();
    }
}
