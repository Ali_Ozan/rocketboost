using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oscillator : MonoBehaviour
{
    Vector3 startingPosition;
    [SerializeField] float period = 2f;
    [SerializeField] Vector3 movementVector;
    float movementFactor;

    void Start()
    {
        startingPosition = transform.position;
    }

    void Update()
    {
        if (period > Mathf.Epsilon) //epsilon is the smallest possible float, period cant be zero or a negative value. 
        {
            float cycles = Time.time / period; //continually growing over time 
            const float tau = Mathf.PI * 2; //constant value of 6.283...
            float rawSinWave = Mathf.Sin(cycles * tau); // going from -1 to 1
            
            movementFactor = (rawSinWave + 1f) / 2f;

            Vector3 offset = movementVector * movementFactor; //recalculated to go from 0 to 1 so its cleaner
            transform.position = startingPosition + offset; 
        } 
    }
}
